/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : diagnostic

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-09-21 15:26:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for a_department
-- ----------------------------
DROP TABLE IF EXISTS `a_department`;
CREATE TABLE `a_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inspection_id` int(11) DEFAULT NULL COMMENT '检验所id',
  `channel_id` int(11) DEFAULT NULL COMMENT '渠道id',
  `name` varchar(255) DEFAULT NULL COMMENT '科室名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='科室表';

-- ----------------------------
-- Table structure for a_dptmentstaff
-- ----------------------------
DROP TABLE IF EXISTS `a_dptmentstaff`;
CREATE TABLE `a_dptmentstaff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inspection_id` varchar(255) DEFAULT NULL COMMENT '机构id（1,2,3....）',
  `nickname` varchar(255) DEFAULT NULL COMMENT '人员名称',
  `account` varchar(255) DEFAULT NULL COMMENT '账号（手机）',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `position` int(11) DEFAULT '1' COMMENT '类型（1医生 2主任 3医院)）',
  `phone` varchar(255) DEFAULT NULL COMMENT '电话',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='科室人员表';

-- ----------------------------
-- Table structure for a_relation_staff_depart
-- ----------------------------
DROP TABLE IF EXISTS `a_relation_staff_depart`;
CREATE TABLE `a_relation_staff_depart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inspection_id` int(11) DEFAULT NULL COMMENT '检验所id',
  `department_id` varchar(255) DEFAULT NULL COMMENT '科室id',
  `staff_id` int(11) DEFAULT NULL COMMENT '人员id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='科室人员及科室对应关系表';

-- ----------------------------
-- Table structure for apilog
-- ----------------------------
DROP TABLE IF EXISTS `apilog`;
CREATE TABLE `apilog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appid` varchar(255) DEFAULT NULL COMMENT 'appid',
  `do` varchar(255) DEFAULT NULL COMMENT '动作(申请哪个接口)',
  `time` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '0为成功',
  `ip` varchar(255) DEFAULT NULL COMMENT '请求的ip地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='api请求记录表';

-- ----------------------------
-- Table structure for channel
-- ----------------------------
DROP TABLE IF EXISTS `channel`;
CREATE TABLE `channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '渠道名称',
  `type_id` int(11) DEFAULT NULL COMMENT '渠道类型id',
  `inspection_id` int(11) DEFAULT NULL COMMENT '检验所id',
  `region_id` int(11) DEFAULT NULL COMMENT '区域id',
  `seq` int(11) DEFAULT NULL COMMENT '排序',
  `phone` varchar(255) DEFAULT NULL COMMENT '电话',
  `mobile` varchar(255) DEFAULT NULL COMMENT '手机',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `web_url` varchar(255) DEFAULT NULL COMMENT '网址',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `sale_account` varchar(255) DEFAULT NULL COMMENT '销售账号',
  `discount` varchar(255) DEFAULT '0' COMMENT '折扣',
  `print_mode` int(11) DEFAULT '1' COMMENT '打印模式 1:无公章 2:有公章',
  `custominfo` varchar(255) DEFAULT NULL COMMENT '自定义信息',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='渠道表';

-- ----------------------------
-- Table structure for channel_user
-- ----------------------------
DROP TABLE IF EXISTS `channel_user`;
CREATE TABLE `channel_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `top_id` int(11) DEFAULT '0' COMMENT '上级id',
  `channel` varchar(255) DEFAULT NULL COMMENT '负责渠道',
  `type` int(11) DEFAULT NULL COMMENT '1 业务员 2 区域经理 3 总监',
  `account` varchar(255) DEFAULT NULL COMMENT '账号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `phone` varchar(255) DEFAULT NULL COMMENT '手机号',
  `qq` varchar(255) DEFAULT NULL COMMENT 'qq号',
  `email` varchar(255) DEFAULT NULL COMMENT '邮件',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `inspection_id` int(11) DEFAULT NULL COMMENT '检测所id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='渠道客户';

-- ----------------------------
-- Table structure for channelprice
-- ----------------------------
DROP TABLE IF EXISTS `channelprice`;
CREATE TABLE `channelprice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) DEFAULT NULL COMMENT '渠道id',
  `projectdes_id` int(11) DEFAULT NULL COMMENT '项目内容id',
  `price` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '价格',
  `price_type` int(11) DEFAULT '1' COMMENT '价格类型（1元 2美元）',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='渠道价格';

-- ----------------------------
-- Table structure for channeltype
-- ----------------------------
DROP TABLE IF EXISTS `channeltype`;
CREATE TABLE `channeltype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inspection_id` int(11) DEFAULT NULL COMMENT '检验所id',
  `name` varchar(255) DEFAULT NULL COMMENT '渠道类型名称',
  `type` int(11) DEFAULT '1' COMMENT '类型 1：个人 2：团体',
  `seq` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='渠道类型';

-- ----------------------------
-- Table structure for cuser
-- ----------------------------
DROP TABLE IF EXISTS `cuser`;
CREATE TABLE `cuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='检测所下用户表包括检验所联系人信息';

-- ----------------------------
-- Table structure for custominfo
-- ----------------------------
DROP TABLE IF EXISTS `custominfo`;
CREATE TABLE `custominfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `examinee_id` int(11) DEFAULT NULL COMMENT '受检人id',
  `name` varchar(255) DEFAULT NULL COMMENT '自定义字段名称',
  `info` varchar(255) DEFAULT NULL COMMENT '自定义字段内容',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='受检人自定义字段表\r\n';

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) DEFAULT NULL COMMENT '渠道id',
  `name` varchar(255) DEFAULT NULL COMMENT '科室名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='科室表';

-- ----------------------------
-- Table structure for dptmentstaff
-- ----------------------------
DROP TABLE IF EXISTS `dptmentstaff`;
CREATE TABLE `dptmentstaff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) DEFAULT NULL COMMENT '渠道id',
  `department_id` int(11) DEFAULT NULL COMMENT '科室id',
  `nickname` varchar(255) DEFAULT NULL COMMENT '科室人员名称',
  `account` varchar(255) DEFAULT NULL COMMENT '账号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `position` int(11) DEFAULT '1' COMMENT '职位 1：医生 2：主任 3：医院',
  `phone` varchar(255) DEFAULT NULL COMMENT '电话',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='科室人员表';

-- ----------------------------
-- Table structure for emailport
-- ----------------------------
DROP TABLE IF EXISTS `emailport`;
CREATE TABLE `emailport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server` varchar(255) DEFAULT NULL COMMENT '服务器',
  `port` varchar(255) DEFAULT NULL COMMENT '端口',
  `agreement` varchar(255) DEFAULT NULL COMMENT '协议',
  `account` varchar(255) DEFAULT NULL COMMENT '账号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `coding` varchar(255) DEFAULT NULL COMMENT '编码',
  `sendemail` varchar(255) DEFAULT NULL COMMENT '发件人邮箱',
  `sendnickname` varchar(255) DEFAULT NULL COMMENT '发件人昵称',
  `replyemail` varchar(255) DEFAULT NULL COMMENT '回复邮箱',
  `replynickname` varchar(255) DEFAULT NULL COMMENT '回复昵称',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='邮件接口设置';

-- ----------------------------
-- Table structure for examinee
-- ----------------------------
DROP TABLE IF EXISTS `examinee`;
CREATE TABLE `examinee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '受检人名称',
  `age` int(11) DEFAULT NULL COMMENT '年龄数字',
  `agetype` int(11) DEFAULT NULL COMMENT '年龄类型 1：年  2：月',
  `ethnic_id` int(11) DEFAULT NULL COMMENT '民族id',
  `area_id` int(11) DEFAULT NULL COMMENT '地区id',
  `sex` int(11) DEFAULT NULL COMMENT '性别 1：男 2：女',
  `contact` varchar(255) DEFAULT NULL COMMENT '联系电话',
  `is_notice` int(11) DEFAULT NULL COMMENT '是否接收通知 0：否 1：是',
  `is_report` int(11) DEFAULT NULL COMMENT '是否通知报告 0：否 1：是',
  `email` varchar(255) DEFAULT NULL,
  `email_is_notice` int(11) DEFAULT NULL COMMENT '邮件是否报告通知 0：否 1：是',
  `qq` varchar(255) DEFAULT NULL,
  `idnum` varchar(255) DEFAULT NULL COMMENT '身份证号',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='受检人表';

-- ----------------------------
-- Table structure for express
-- ----------------------------
DROP TABLE IF EXISTS `express`;
CREATE TABLE `express` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `temp` longtext COMMENT '快递单模板',
  `inspection_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT '模板名称/备注',
  `last_change_time` varchar(255) DEFAULT NULL,
  `last_user_id` int(11) DEFAULT NULL,
  `default` int(11) DEFAULT '0' COMMENT '默认快递单模板（1 ：默认  0非默认）',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='快递单模板表';

-- ----------------------------
-- Table structure for formulalibrary
-- ----------------------------
DROP TABLE IF EXISTS `formulalibrary`;
CREATE TABLE `formulalibrary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `formula_name` varchar(255) DEFAULT NULL,
  `formula` longtext,
  `remark` longtext,
  `formula_arr` longtext,
  `result` longtext,
  `result_alias` longtext COMMENT '别名',
  `field` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=111 DEFAULT CHARSET=utf8 COMMENT='公式库';

-- ----------------------------
-- Table structure for generate
-- ----------------------------
DROP TABLE IF EXISTS `generate`;
CREATE TABLE `generate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) DEFAULT NULL COMMENT '渠道id',
  `inspection_id` int(11) DEFAULT NULL COMMENT '检验所id',
  `status` int(11) DEFAULT '0' COMMENT '0刚创建1已提交至送检信息管理',
  `projectdes_id` varchar(255) DEFAULT NULL COMMENT '项目内容id 数组',
  `insptype_id` int(11) DEFAULT NULL COMMENT '检验类别id',
  `number` int(11) DEFAULT '1' COMMENT '数量',
  `printnum` int(11) DEFAULT NULL COMMENT '条码打印数量',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='生成条码页面';

-- ----------------------------
-- Table structure for gprfile
-- ----------------------------
DROP TABLE IF EXISTS `gprfile`;
CREATE TABLE `gprfile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(255) DEFAULT NULL COMMENT '图片文件路径',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for inspection
-- ----------------------------
DROP TABLE IF EXISTS `inspection`;
CREATE TABLE `inspection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '检验所名称',
  `address` varchar(255) DEFAULT NULL COMMENT '检验所地址',
  `contact` varchar(255) DEFAULT NULL COMMENT '检验所联系方式',
  `email` varchar(255) DEFAULT NULL COMMENT '检验所联系邮箱',
  `timezone` varchar(255) DEFAULT NULL COMMENT '时区',
  `region_id` int(11) DEFAULT NULL COMMENT '区域id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COMMENT='检测所表';

-- ----------------------------
-- Table structure for inspectiontype
-- ----------------------------
DROP TABLE IF EXISTS `inspectiontype`;
CREATE TABLE `inspectiontype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inspection_id` int(11) DEFAULT NULL COMMENT '检验所id',
  `name` varchar(255) DEFAULT NULL COMMENT '检测类别名称',
  `des` varchar(255) DEFAULT NULL COMMENT 'j检测类别备注',
  `is_default` int(11) DEFAULT '0' COMMENT '是否为默认0否1是',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='检测类型表';

-- ----------------------------
-- Table structure for interfile
-- ----------------------------
DROP TABLE IF EXISTS `interfile`;
CREATE TABLE `interfile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(255) DEFAULT NULL COMMENT '图片文件路径',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for mailtmp
-- ----------------------------
DROP TABLE IF EXISTS `mailtmp`;
CREATE TABLE `mailtmp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `temp` longtext,
  `last_change_time` varchar(255) DEFAULT NULL,
  `last_user_id` int(11) DEFAULT NULL,
  `default` int(11) DEFAULT '0' COMMENT '1为默认',
  `inspection_id` int(11) DEFAULT '0' COMMENT '机构id',
  `name` varchar(255) DEFAULT NULL COMMENT '模板名称',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='邮件模板';

-- ----------------------------
-- Table structure for mygroup
-- ----------------------------
DROP TABLE IF EXISTS `mygroup`;
CREATE TABLE `mygroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inspection_id` int(11) DEFAULT NULL COMMENT '对应检验所id 既用户id',
  `name` varchar(255) DEFAULT NULL COMMENT '分组名称',
  `intro` varchar(255) DEFAULT NULL COMMENT '备注',
  `role_arr` text COMMENT '权限数组',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='总检验所下分组表';

-- ----------------------------
-- Table structure for nation
-- ----------------------------
DROP TABLE IF EXISTS `nation`;
CREATE TABLE `nation` (
  `id` int(2) NOT NULL,
  `nation_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='民族表';

-- ----------------------------
-- Table structure for physical
-- ----------------------------
DROP TABLE IF EXISTS `physical`;
CREATE TABLE `physical` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inspection_id` int(11) DEFAULT NULL COMMENT '检验所id',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `scope` int(11) DEFAULT '1' COMMENT '适用范围 1：男 2：女 3：通用',
  `seq` varchar(255) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='身体状况（身体适用）';

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codenum` varchar(255) DEFAULT NULL COMMENT '检验单编号',
  `examinee_id` int(11) DEFAULT NULL COMMENT '受检人id',
  `channel_id` int(11) DEFAULT NULL COMMENT '渠道id',
  `department_id` int(11) DEFAULT NULL COMMENT '科室id',
  `dptmentstaff_id` int(11) DEFAULT NULL COMMENT '申请人 / 科室人员 id',
  `hospitalnum` varchar(255) DEFAULT NULL COMMENT '住院号2',
  `bednum` varchar(255) DEFAULT NULL COMMENT '床号',
  `clinicalfind` varchar(255) DEFAULT NULL COMMENT '临床所见',
  `projectpic` text COMMENT '送检图片，序列化数组形式存储',
  `interfile` varchar(255) DEFAULT NULL COMMENT '判读文件 对应判读文件表',
  `create_time` varchar(255) DEFAULT NULL COMMENT '接收时间',
  `visit_time` varchar(255) DEFAULT NULL COMMENT '就诊时间',
  `sampling_time` varchar(255) DEFAULT NULL COMMENT '采样时间',
  `end_time` varchar(255) DEFAULT NULL COMMENT '结束时间（报告时间）',
  `inspection_id` int(11) DEFAULT NULL COMMENT '检验所id',
  `status` int(11) DEFAULT '0' COMMENT '该检验进度 0:刚创建 1:送检信息管理 2:检验结果管理 3:审核报告管理 4:报告授权管理 5:完成',
  `projectdes_id` varchar(255) DEFAULT NULL COMMENT '项目内容id',
  `insptype_id` int(11) DEFAULT NULL COMMENT '检验所类型id',
  `physical_id` varchar(255) DEFAULT NULL COMMENT '身体状况id 数组',
  `physical_remark` varchar(255) DEFAULT NULL COMMENT '身体备注',
  `onecode` varchar(255) DEFAULT NULL COMMENT '一维码',
  `printnum` int(11) DEFAULT NULL COMMENT '条码打印份数',
  `group_id` int(11) DEFAULT NULL COMMENT '所属组id',
  `download_num` int(11) DEFAULT '0' COMMENT '下载次数',
  PRIMARY KEY (`id`),
  KEY `受检人索引` (`id`,`examinee_id`) USING BTREE,
  KEY `科室索引` (`id`,`department_id`,`dptmentstaff_id`) USING BTREE,
  KEY `渠道索引` (`id`,`channel_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COMMENT='检验单主表';

-- ----------------------------
-- Table structure for projectdes
-- ----------------------------
DROP TABLE IF EXISTS `projectdes`;
CREATE TABLE `projectdes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL COMMENT '类型id',
  `personalname` varchar(255) DEFAULT NULL COMMENT '个人项目内容名称',
  `clinicalname` varchar(255) DEFAULT NULL COMMENT '临床项目内容名称',
  `number` varchar(255) DEFAULT NULL COMMENT '编号',
  `code` varchar(255) DEFAULT NULL COMMENT '代号',
  `info` varchar(255) DEFAULT NULL COMMENT '项目内容',
  `intro` varchar(255) DEFAULT NULL COMMENT '项目内容简介',
  `intro_html` varchar(255) DEFAULT NULL COMMENT '简介html',
  `seq` int(11) DEFAULT '0' COMMENT '排序',
  `is_used` int(11) DEFAULT '1' COMMENT '0-不启用 1-启用',
  `inspection_id` int(11) DEFAULT NULL COMMENT '检验所id',
  `formula_id` int(11) DEFAULT NULL COMMENT '公式id',
  `template_id` varchar(255) DEFAULT NULL COMMENT '模板id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='项目内容表';

-- ----------------------------
-- Table structure for projectdes_child
-- ----------------------------
DROP TABLE IF EXISTS `projectdes_child`;
CREATE TABLE `projectdes_child` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectdes_id` int(11) DEFAULT NULL,
  `inspection_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='分检验所项目内容管理';

-- ----------------------------
-- Table structure for projectpic
-- ----------------------------
DROP TABLE IF EXISTS `projectpic`;
CREATE TABLE `projectpic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(255) DEFAULT NULL COMMENT '文件路径',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COMMENT='送检照片表';

-- ----------------------------
-- Table structure for projecttype
-- ----------------------------
DROP TABLE IF EXISTS `projecttype`;
CREATE TABLE `projecttype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '项目类型名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `is_formula` int(11) DEFAULT '0' COMMENT '是否显示公式1：是 0：否',
  `seq` varchar(255) DEFAULT NULL COMMENT '排序',
  `inspection_id` int(11) DEFAULT NULL COMMENT '检验所id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='体检项目类型表';

-- ----------------------------
-- Table structure for region
-- ----------------------------
DROP TABLE IF EXISTS `region`;
CREATE TABLE `region` (
  `region_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `region_name` varchar(120) DEFAULT '',
  `region_type` tinyint(1) NOT NULL DEFAULT '2',
  `agency_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`region_id`),
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `region_type` (`region_type`) USING BTREE,
  KEY `agency_id` (`agency_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3490 DEFAULT CHARSET=utf8 COMMENT='全国地区信息';

-- ----------------------------
-- Table structure for relation_template_projectdes
-- ----------------------------
DROP TABLE IF EXISTS `relation_template_projectdes`;
CREATE TABLE `relation_template_projectdes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) DEFAULT NULL COMMENT '模板id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `inspection_id` int(11) DEFAULT NULL COMMENT '检验所id',
  `keyword` varchar(255) DEFAULT NULL COMMENT '自定义关键字',
  `remark` varchar(255) DEFAULT NULL COMMENT '自定义关键字备注',
  `result` varchar(255) DEFAULT NULL COMMENT '绑定值',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='模板与项目关系表';

-- ----------------------------
-- Table structure for report_field
-- ----------------------------
DROP TABLE IF EXISTS `report_field`;
CREATE TABLE `report_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) DEFAULT NULL COMMENT '表名',
  `field_name` varchar(255) DEFAULT NULL COMMENT '字段名',
  `type` int(11) DEFAULT NULL COMMENT '类型（1封面 2自定义 3项目 4尾部 5判读公式）',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `formula_id` int(11) DEFAULT NULL COMMENT '公式id',
  `inspection_id` int(11) DEFAULT NULL COMMENT '检验所id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=131 DEFAULT CHARSET=utf8 COMMENT='参数表';

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '权限表ID，对应user表内role的数组内容',
  `parent_id` int(11) DEFAULT NULL COMMENT '上级权限功能id',
  `role_name` varchar(255) DEFAULT NULL COMMENT '权限名称/可使用功能',
  `role_name_en` varchar(255) DEFAULT NULL COMMENT '权限名称英文',
  `role_url` varchar(255) DEFAULT NULL COMMENT '权限功能路径',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=172 DEFAULT CHARSET=utf8 COMMENT='暂定只有检测功能列表，之后添加如公式增加等功能权限列表';

-- ----------------------------
-- Table structure for samplestatus
-- ----------------------------
DROP TABLE IF EXISTS `samplestatus`;
CREATE TABLE `samplestatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inspection_id` int(11) DEFAULT NULL COMMENT '检验所id',
  `name` varchar(255) DEFAULT NULL COMMENT '样本状态名称',
  `seq` varchar(255) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='样本状态表';

-- ----------------------------
-- Table structure for sampletype
-- ----------------------------
DROP TABLE IF EXISTS `sampletype`;
CREATE TABLE `sampletype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inspection_id` int(11) DEFAULT NULL COMMENT '检验所id',
  `name` varchar(255) DEFAULT NULL COMMENT '样本类型名称',
  `intro` varchar(255) DEFAULT NULL COMMENT '样本类型简介',
  `seq` varchar(255) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='标本/样本 类型表';

-- ----------------------------
-- Table structure for shopapp
-- ----------------------------
DROP TABLE IF EXISTS `shopapp`;
CREATE TABLE `shopapp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appid` varchar(255) DEFAULT NULL COMMENT 'appid',
  `secret` varchar(255) DEFAULT NULL COMMENT 'secret',
  `name` varchar(255) DEFAULT NULL COMMENT 'app名称',
  `create_time` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `last_time` varchar(255) DEFAULT NULL COMMENT '最后申请时间',
  `today_num` int(11) DEFAULT '0' COMMENT '今日申请次数',
  `status` int(11) DEFAULT '0' COMMENT '是否禁用 1 是 0 否',
  `ishow` int(11) DEFAULT NULL COMMENT '是否显示？',
  `token` varchar(255) DEFAULT NULL COMMENT '生成token',
  `token_time` varchar(255) DEFAULT NULL COMMENT '生成token的时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='接口appid与secret';

-- ----------------------------
-- Table structure for sms
-- ----------------------------
DROP TABLE IF EXISTS `sms`;
CREATE TABLE `sms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `func_name` varchar(255) DEFAULT NULL COMMENT '触发函数',
  `template` int(11) DEFAULT NULL COMMENT '模板ID',
  `remark` text COMMENT '备注-标记',
  `is_used` int(11) DEFAULT '1' COMMENT '启用状态（1启用 2不启用）',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='短信接口';

-- ----------------------------
-- Table structure for subproject
-- ----------------------------
DROP TABLE IF EXISTS `subproject`;
CREATE TABLE `subproject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL COMMENT '父级检测项目id',
  `projecttype_id` int(11) DEFAULT NULL COMMENT '检测类别',
  `projectdes_id` int(11) DEFAULT NULL COMMENT 'j检测项目',
  `sampletype` int(11) DEFAULT NULL COMMENT '样本类型',
  `samplestatus` int(11) DEFAULT NULL COMMENT '样本状态',
  `sampleremark` varchar(255) DEFAULT NULL COMMENT '样本备注',
  `samplecode` varchar(255) DEFAULT NULL COMMENT '样本编号',
  `codenum` varchar(255) DEFAULT NULL COMMENT '编号',
  `barcode` varchar(255) DEFAULT NULL COMMENT '条码',
  `complete_time` varchar(255) DEFAULT NULL COMMENT '完成时间',
  `interfile` int(11) DEFAULT NULL COMMENT '判读图片',
  `gprfile` int(11) DEFAULT NULL COMMENT 'gpr数据文件id',
  `inspection_id` int(11) DEFAULT NULL COMMENT '检测所id',
  `status` int(11) DEFAULT NULL COMMENT '状态  0:刚创建 1:送检信息管理 2:检验结果管理 3:审核报告管理 4:报告授权管理 5:完成',
  `result` longtext COMMENT '判读信息保存',
  `create_time` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `insptype_id` int(11) DEFAULT NULL COMMENT '检测类型id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='子检测项目表';

-- ----------------------------
-- Table structure for table_field
-- ----------------------------
DROP TABLE IF EXISTS `table_field`;
CREATE TABLE `table_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) DEFAULT NULL,
  `field_name` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `object_id` int(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `formula_name` varchar(255) DEFAULT NULL,
  `formula_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6779 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for template
-- ----------------------------
DROP TABLE IF EXISTS `template`;
CREATE TABLE `template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inspection_id` int(11) DEFAULT NULL COMMENT '检验所id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id（0：检验所总账号）',
  `name` varchar(255) DEFAULT NULL COMMENT '报告模板名称',
  `report` mediumtext COMMENT '模板内容',
  `version_num` varchar(255) DEFAULT NULL COMMENT '版本号',
  `p_id` int(11) DEFAULT '0' COMMENT '父模板id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='模板';

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户表ID，用户表索引',
  `inspection_id` int(11) DEFAULT NULL COMMENT '上级用户id',
  `top_id` int(11) DEFAULT NULL COMMENT '所属检验所id',
  `group_id` int(11) DEFAULT NULL COMMENT '分组id',
  `account` varchar(255) DEFAULT NULL COMMENT '登录账号',
  `password` varchar(255) DEFAULT NULL COMMENT '登录密码',
  `contact` varchar(255) DEFAULT NULL COMMENT '联系方式/联系电话',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `is_use` int(11) DEFAULT '0' COMMENT '0启用 1未启用',
  `is_admin` int(11) DEFAULT '0' COMMENT '是否是超管 1是 0不是',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COMMENT='所有后台用户信息及权限表';
